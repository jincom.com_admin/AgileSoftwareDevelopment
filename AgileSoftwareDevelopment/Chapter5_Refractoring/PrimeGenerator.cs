using System;

namespace AgileSoftwareDevelopment.Chapter5_Refractoring
{
    public class PrimeGenerator
    {
        public static int[] GeneratePrimes(int maxValue)
        {
            if (maxValue >= 2)
            {
                bool[] b = new bool[maxValue + 1];

                for (int i = 0; i < b.Length; i++)
                {
                    b[i] = true;
                }

                b[0] = b[1] = false;
                
                for(int i = 2; i < Math.Sqrt(b.Length); i++)
                {
                    for (int j = 2 * i; j < b.Length; j += i)
                    {
                        b[j] = false;
                    }
                }

                int count = 0;
                for (int i = 0; i < b.Length; i++)
                {
                    if (b[i])
                        count++;
                }

                int[] primes = new int[count];
                int primeIndex = 0;
                for (int i = 0; i < b.Length; i++)
                {
                    if (b[i])
                        primes[primeIndex++] = i;

                }

                return primes;
            }

            return new int[0];
        }
    }
}