using AgileSoftwareDevelopment.Chapter5_Refractoring;
using NUnit.Framework;

namespace AgileSoftwareDevelopmentTest.Chapter5_RefractoringTest
{
    [TestFixture]
    public class PrimeGeneratorTest
    {
        [Test]
        public void GeneratePrimes_MaxValue0_Count_0()
        {
            int[] primes = PrimeGenerator.GeneratePrimes(0);
            Assert.True(primes.Length == 0);
        }
        
        [Test]
        public void GeneratePrimes_MaxValue2_Count_1()
        {
            int[] primes = PrimeGenerator.GeneratePrimes(2);
            Assert.True(primes.Length == 1);
            Assert.AreEqual(primes[0], 2);
        }
        
        [Test]
        public void GeneratePrimes_MaxValue100_Count_25()
        {
            int[] primes = PrimeGenerator.GeneratePrimes(100);
            Assert.True(primes.Length == 25);
            Assert.AreEqual(primes[24], 97);
        }

        [Test]
        public void GeneratePrimes_MaxValue500_NoMultiples()
        {
            int[] primes = PrimeGenerator.GeneratePrimes(500);

            for (int i = 0; i < primes.Length; i++)
            {
                VerifyPrime(primes[i]);
            }
        }

        private void VerifyPrime(int prime)
        {
            for (int i = 2; i < prime; i++)
            {
                Assert.AreNotEqual(prime % i, 0); 
            }
        }
    }
}